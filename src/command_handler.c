// Handler function to either pass through RX commands to Naza or else
// copy computer (autonomous control) commands through to Naza.
#define EXTERN extern

#include "../include/quadcopter_main.h"

////////////////////////////////////////////////////////////////////////

void channels_handler(const lcm_recv_buf_t *rbuf, const char *channel,
		      const channels_t *msg, void *userdata)
{
  // create a copy of the received message
  channels_t new_msg;
  new_msg.utime = msg->utime;
  new_msg.num_channels = msg->num_channels;
  new_msg.channels = (int16_t*) malloc(msg->num_channels*sizeof(int16_t));
  int16_t* channels_tmp = (int16_t*) malloc(4*sizeof(int16_t));

  for(int i = 0; i < msg->num_channels; i++){
    new_msg.channels[i] = msg->channels[i];
  }
  // need to change the value
  if (msg->channels[7] > 1500) {
    state->fence_on = 1;
  } else {
    state->fence_on = 0;
  }
  // Copy state to local state struct to minimize mutex lock time
  struct state localstate;
  pthread_mutex_lock(&state_mutex);
  memcpy(&localstate, state, sizeof(struct state));
  pthread_mutex_unlock(&state_mutex);

  // Decide whether or not to edit the motor message prior to sending it
  // set_points[] array is specific to geofencing.  You need to add code 
  // to compute them for our FlightLab application!!!
  float pose[8], set_points[8];
  if(localstate.fence_on == 1) {
    
    //    pose[2] = localstate.pose[2];
    //for(int i = 0; i < 8; i++){
    //pose[i] = (float) localstate.pose[i];
    //set_points[i] = localstate.set_points[i];
    
    //}
    //printf("x,y,z: %f, %f, %f\n",localstate.pose[0], localstate.pose[1], localstate.pose[2]);
    pose[0] = localstate.pose[0];
    pose[1] = localstate.pose[1];
    pose[2] = localstate.pose[2];
    pose[3] = localstate.pose[3];
    pose[4] = localstate.pose[4];
    pose[5] = localstate.pose[5];
    pose[6] = localstate.pose[6];
    pose[7] = localstate.pose[7];

    // printf("x,y,z: %f, %f, %f\n", pose[0],pose[1],pose[2]);
    //printf("set_points x,y,z:%f, %f, %f\n", localstate.set_points[0], localstate.set_points[1], localstate.set_points[2]);
    // hold position at edge of fence
    // This needs to change - mutex held way too long
    
    //if find optitrack data lost, set to neural position
    // if data_lost = 1 means optitrack data drop out
    //test purpose
 
    if (localstate.data_lost == 0) {
      auto_control(pose, (localstate.set_points), channels_tmp);

    //height first
      //printf("channels 1:%d\n", channels_tmp[1]);
      
      new_msg.channels[0] = channels_tmp[0];
      new_msg.channels[1] = channels_tmp[1];
      new_msg.channels[2] = channels_tmp[2];
      //new_msg.channels[3] = channels_tmp[3];
      new_msg.channels[7] = 1180;
    } else {

      new_msg.channels[0] = thrust_PWM_base;
      new_msg.channels[1] = roll_PWM_base;
      new_msg.channels[2] = pitch_PWM_base;
      //new_msg.channels[3] = yaw_PWM_base;
      new_msg.channels[7] = 1180;
      printf("data dropoff\n");

    }


  } else {  // Fence off
    
    // pass user commands through without modifying
    printf("FENCE OFF\n");

    x_i_err =0.0;
    y_i_err =0.0;
    alt_i_err= 0.0;

  }

  // send lcm message to motors
  channels_t_publish((lcm_t *) userdata, "CHANNELS_1_TX", &new_msg);

  // Save received (msg) and modified (new_msg) command data to file.
  // NOTE:  Customize as needed (set_points[] is for geofencing)
}
