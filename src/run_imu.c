#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <unistd.h>
#include <pthread.h>

#define EXTERN extern
#define IMU_FREQ 50 // in Hz
#include "../include/quadcopter_main.h"
 
float GYROX_BIAS = 0.0, GYROY_BIAS = 0.0, GYROZ_BIAS=0.0;
float ACCEL_X_BIAS = 0.0, ACCEL_Y_BIAS = 0.0, ACCEL_Z_BIAS = 0.0;

void *run_imu(void * var)
{
  IMU_calibration();
  printf("BIAS:%f, %f, %f, %f, %f, %f\n", GYROX_BIAS, GYROY_BIAS, GYROZ_BIAS, ACCEL_X_BIAS, ACCEL_Y_BIAS, ACCEL_Z_BIAS);
  struct I2C_data gyro;
  struct I2C_data accel;
  int hz = IMU_FREQ;

  if(init_imu(&gyro,&accel) == -1) return NULL;
  imu_initialize_data();

  double gyro_data[3], accel_data[3];
  imu_data_t imu_msg;

  while(1){
    read_gyro(&gyro, gyro_data);
    read_accel(&accel, accel_data);

    pthread_mutex_lock(&imu_mutex);
    imudata.utime = utime_now();
    imudata.gyro_x = gyro_data[0];
    imudata.gyro_y = gyro_data[1];
    imudata.gyro_z = gyro_data[2];
    imudata.accel_x = accel_data[0];
    imudata.accel_y = accel_data[1];
    imudata.accel_z = accel_data[2];

    imu_msg.accel[0] = imudata.accel_x;
    imu_msg.accel[1] = imudata.accel_y;
    imu_msg.accel[2] = imudata.accel_z;

    imu_msg.gyro[0] = imudata.gyro_x;
    imu_msg.gyro[1] = imudata.gyro_y;
    imu_msg.gyro[2] = imudata.gyro_z;

    //    for (int kkk; kkk <3; kkk++) {
    // imu_msg.accel[kkk] = accel_data[kkk];
    // imu_msg.gyro[kkk] = gyro_data[kkk];
    //}


    pthread_mutex_unlock(&imu_mutex);
    imu_data_t_publish(lcm, "IMU_DATA", &imu_msg);
    
    //    printf("%ld,%lf,%lf,%lf,%lf,%lf,%lf\n",
    //	   (long int) imudata.utime,imudata.gyro_x,imudata.gyro_y,imudata.gyro_z,
    //	   imudata.accel_x,imudata.accel_y,imudata.accel_z);
    // fprintf(imu_txt,"%ld,%lf,%lf,%lf,%lf,%lf,%lf\n",
    //	    (long int) imudata.utime,imudata.gyro_x,imudata.gyro_y,imudata.gyro_z,
    //	    imudata.accel_x,imudata.accel_y,imudata.accel_z);
    //fflush(imu_txt);

    usleep(1000000/hz);

  }

  IMU_destroy(&gyro,&accel);
  fclose(imu_txt);
  return NULL;
}

int init_imu(struct I2C_data* gyro, struct I2C_data* accel){
  if(bbb_init() == -1){
    printf("Error initializing BBB.\n");
    return -1;
  }

  gyro->name = I2C_1;
  gyro->address = I2C_GYRO_ADDRESS;
  gyro->flags = O_RDWR;

  accel->name = I2C_1;
  accel->address = I2C_ACCEL_ADDRESS;
  accel->flags = O_RDWR;

  if(bbb_initI2C(gyro) == -1){
    printf("Error initializing I2C port for gyroscope.\n");
    return -1;
  }

  if(bbb_initI2C(accel) == -1){
    printf("Error initializing I2C port for accel/mag.\n");
    return -1;
  }

  byte buf[10];

  buf[0] = 0x20;
  buf[1] = 0x0F;
  bbb_writeI2C(gyro, buf, 2);

  buf[0] = 0x20;
  buf[1] = 0x67;
  bbb_writeI2C(accel, buf, 2);
  buf[0] = 0x24;
  buf[1] = 0xF0;
  bbb_writeI2C(accel, buf, 2);
  buf[0] = 0x26;
  buf[1] = 0x00;
  bbb_writeI2C(accel, buf, 2);

  return 0;
}

int imu_initialize_data()
{
  imu = (imu_data*) malloc(sizeof(struct imu_data));

  memset(imu->gyro_x_r,0,sizeof(imu->gyro_x_r));
  memset(imu->gyro_y_r,0,sizeof(imu->gyro_y_r));
  memset(imu->gyro_z_r,0,sizeof(imu->gyro_z_r));
  memset(imu->gyro_x_m,0,sizeof(imu->gyro_x_m));
  memset(imu->gyro_y_m,0,sizeof(imu->gyro_y_m));
  memset(imu->gyro_z_m,0,sizeof(imu->gyro_z_m));
  memset(imu->gyro_x,0,sizeof(imu->gyro_x));
  memset(imu->gyro_y,0,sizeof(imu->gyro_y));
  memset(imu->gyro_z,0,sizeof(imu->gyro_z));

  memset(imu->accel_x_r,0,sizeof(imu->accel_x_r));
  memset(imu->accel_y_r,0,sizeof(imu->accel_y_r));
  memset(imu->accel_z_r,0,sizeof(imu->accel_z_r));
  memset(imu->accel_x_m,0,sizeof(imu->accel_x_m));
  memset(imu->accel_y_m,0,sizeof(imu->accel_y_m));
  memset(imu->accel_z_m,0,sizeof(imu->accel_z_m));
  memset(imu->accel_x,0,sizeof(imu->accel_x));
  memset(imu->accel_y,0,sizeof(imu->accel_y));
  memset(imu->accel_z,0,sizeof(imu->accel_z));

  imu_txt = fopen("imu.txt","a");

  return 0;
}


void read_gyro(struct I2C_data *i2cd, double gyro[]){
  short tempint;
  byte buf, lobyte, hibyte;  // Used to store I2C data
  
  buf = 0x28; // X gyro, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x29; // X gyro, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (((short) hibyte) << 8) | lobyte;

  // GYROX_BIAS is the zero bias/offset - please adjust to make tempint close to zero for your x-gyro
  // printf("gyro x=%hd\n", tempint+GYROX_BIAS); // With your bias this should be near zero
  gyro[0] = 0.00875*(tempint) - GYROX_BIAS;  // 110 is the zero bias/offset - please adjust
  
  buf = 0x2A; // Y gyro, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x2B; // Y gyro, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (((short) hibyte) << 8) | lobyte;

  // GYROY_BIAS is the zero bias/offset - please adjust to make tempint close to zero for your y-gyro
  // printf("gyro y=%hd\n", tempint + GYROY_BIAS); // With your bias this should be near zero
  gyro[1] = 0.00875*(tempint) - GYROY_BIAS;  
  
  buf = 0x2C; // Z gyro, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x2D; // Z gyro, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = ((short) hibyte << 8) | lobyte;

  // GYROZ_BIAS is the zero bias/offset - please adjust to make tempint close to zero for your y-gyro
  // printf("gyro z=%hd\n", tempint + GYROZ_BIAS); // With your bias this should be near zero
  gyro[2] = 0.00875*(tempint) - GYROZ_BIAS;

  return;
}


void read_accel(struct I2C_data *i2cd, double accel[]){
  short tempint;
  byte buf, lobyte, hibyte;  // Used to store I2C data
  
  buf = 0x28; // X accel, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x29; // X accel, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (((short) hibyte) << 8) | lobyte;
  accel[0] = 0.000061*tempint - ACCEL_X_BIAS;
  
  buf = 0x2A; // Y accel, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x2B; // Y accel, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (((short) hibyte) << 8) | lobyte;
  accel[1] = 0.000061*tempint - ACCEL_Y_BIAS;
  
  buf = 0x2C; // Z accel, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x2D; // Z accel, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (((short) hibyte) << 8) | lobyte;
  accel[2] = 0.000061*tempint - ACCEL_Z_BIAS; 

  return;
}


void IMU_destroy(struct I2C_data *i2cd_gyro,struct I2C_data *i2cd_accelmag){
  bbb_deinitI2C(i2cd_gyro);
  bbb_deinitI2C(i2cd_accelmag);
}

void IMU_calibration() {
  
  struct I2C_data gyro;
  struct I2C_data accel;
  int hz = IMU_FREQ;

  if(init_imu(&gyro,&accel) == -1) return;
  imu_initialize_data();

  double gyro_data[3], accel_data[3];
  double gyro_x_acc = 0, gyro_y_acc = 0, gyro_z_acc = 0;
  double accel_x_acc = 0, accel_y_acc = 0, accel_z_acc = 0;
  long int counter = 0;
  
  printf("start calibration\n");
  while(1){
    
    read_gyro(&gyro, gyro_data);
    read_accel(&accel, accel_data);

    gyro_x_acc += gyro_data[0];
    gyro_y_acc += gyro_data[1];
    gyro_z_acc += gyro_data[2];
    
    accel_x_acc += accel_data[0];
    accel_y_acc += accel_data[1];
    accel_z_acc += accel_data[2] - 1;

    usleep(1000000/hz);
    counter ++;
    if(counter > 1000)
      break;
  }
  
  GYROX_BIAS = gyro_x_acc / (float) counter;
  GYROY_BIAS = gyro_y_acc / (float) counter;
  GYROZ_BIAS = gyro_z_acc / (float) counter;
  
  ACCEL_X_BIAS = accel_x_acc / (float) counter;
  ACCEL_Y_BIAS = accel_y_acc / (float) counter;
  ACCEL_Z_BIAS = accel_z_acc / (float) counter;

  printf("calibration done \n");

}
