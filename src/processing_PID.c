// Processing loop for testing PID controller
// Only one set point are setted
#define EXTERN extern
#define PROC_FREQ 100 // in Hz

#define norm_square_thres 0.1
#define yaw_thres 0.15
#include "../include/quadcopter_main.h"
int processing_loop_initialize2();
/*
 * State estimation and guidance thread 
*/
void *processing_PID(void *data){
  printf("%d\n", __LINE__);
  int hz = PROC_FREQ;
  processing_loop_initialize2();
  printf("%d\n", __LINE__);

  // Local copies
  struct motion_capture_obs *mcobs[2];  // [0] = new, [1] = old
  struct state localstate;
  
  //initialize first way point
  // if no way point has been set
  if (set_wypt_index == 0) {
    for (int i = 0; i < 4; i ++)
      localstate.set_points[i] = 0;
  } else {
        update_set_points(localstate.pose, localstate.set_points,1);
  }
  printf("%d\n", __LINE__);
  while (1) {  // Main thread loop

    pthread_mutex_lock(&mcap_mutex);
    mcobs[0] = mcap_obs;
    if(mcap_obs[1].time > 0) mcobs[1] = mcap_obs+1;
    else               mcobs[1] = mcap_obs;

    //check if it's in auto mode
    localstate.fence_on = state->fence_on;
    
    pthread_mutex_unlock(&mcap_mutex);
  
    localstate.time = mcobs[0]->time;
    localstate.pose[0] = mcobs[0]->pose[0]; // x position
    localstate.pose[1] = mcobs[0]->pose[1]; // y position
    localstate.pose[2] = mcobs[0]->pose[2]; // z position / altitude
    localstate.pose[3] = mcobs[0]->pose[5]; // yaw angle
    localstate.pose[4] = 0;
    localstate.pose[5] = 0;
    localstate.pose[6] = 0;
    localstate.pose[7] = 0;
    

    // Estimate velocities from first-order differentiation
    double mc_time_step = mcobs[0]->time - mcobs[1]->time;
    //    printf("dt : %f\n", mc_time_step);
    if(mc_time_step > 1.0E-7 && mc_time_step < 1) {
      
      localstate.pose[4] = (mcobs[0]->pose[0]-mcobs[1]->pose[0])/mc_time_step;
      localstate.pose[5] = (mcobs[0]->pose[1]-mcobs[1]->pose[1])/mc_time_step;
      localstate.pose[6] = (mcobs[0]->pose[2]-mcobs[1]->pose[2])/mc_time_step;
      localstate.pose[7] = (mcobs[0]->pose[5]-mcobs[1]->pose[5])/mc_time_step;

      localstate.data_lost = 0;
    
    } else {
      //if time_step greater than 1s, optitrack data drops off
      localstate.data_lost = 1;
      printf("lost!\n");

    }

  // Geofence activation code
  // turn fence on --> may be useful to control your autonomous controll

    // else if fence is on and has been on for desired length of time

    //only read here
    if(localstate.fence_on == 1){
      // update desired state
      // check if need to update setpoints
      //printf("Fence On\n");

        localstate.set_points[0] = 0;
        localstate.set_points[1] = 0;
        localstate.set_points[2] = -1.2;
        localstate.set_points[3] = 0;
  
    }

    // Copy to global state (minimize total time state is locked by the mutex)
    pthread_mutex_lock(&state_mutex);
    // copy the state to the global variable
    memcpy(state, &localstate, sizeof(struct state));
    
    
    pthread_mutex_unlock(&state_mutex);

    usleep(1000000/hz);

  } 

  return 0;
}

int processing_loop_initialize2() {
  
  // Initialize state struct
  state = (state_t*) calloc(1,sizeof(*state));
  state->time = ((double)utime_now())/1000000;
  memset(state->pose,0,sizeof(state->pose));
  printf("%d\n", __LINE__);

  // // Read configuration file
  // char blah[] = "config.txt";
  // read_config(blah);
  printf("%d\n", __LINE__);

  mcap_obs[0].time = state->time;
  mcap_obs[1].time = -1.0;  // Signal that this isn't set yet
  printf("%d\n", __LINE__);

  // Initialize Altitude Velocity Data Structures
  memset(diff_z, 0, sizeof(diff_z));
  memset(diff_z_med, 0, sizeof(diff_z_med));

  // Fence variables
  state->fence_on = 0;
  memset(state->set_points,0,sizeof(state->set_points));
  state->time_fence_init = 0;
  waypoint_index = 0;
  set_wypt_index = 0;

  //initialized integral terms in PID
  x_i_err = 0;
  y_i_err = 0;
  alt_i_err = 0;

  // boolean
  grip = 0;
  release = 0;
  // Initialize IMU data
  //if(imu_mode == 'u' || imu_mode == 'r'){
  //  imu_initialize();
  //}

  return 0;

}
