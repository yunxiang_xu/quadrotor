//
// auto_control:  Function to generate autonomous control PWM outputs.
// 
#define EXTERN extern
#include "../include/quadcopter_main.h"

#define GAIN_X 2.4142135
#define GAIN_Y 2.4142135
#define GAIN_Z 2.4142135
#define GAIN_X_DOT 2.210
#define GAIN_Y_DOT 2.210
#define GAIN_Z_DOT 2.210

#define GAIN_X_I 0.6
#define GAIN_Y_I 0.8
#define GAIN_Z_I 0.8

#define x_i_thres 0.8
#define y_i_thres 1.3
#define z_i_thres 1.3

#define GAIN_YAW 1.0
#define A_z -60.0
#define A_r -150.0
#define A_p 200.0
#define A_yaw -60

// Outer loop controller to generate PWM signals for the Naza-M autopilot
void auto_control(float *pose, float *set_points, int16_t* channels_ptr) {  
  
  // pose (size 8):  actual {x, y , alt, yaw, xdot, ydot, altdot, yawdot}
  // set_points (size 8):  reference state (you need to set this!) 
  //                       {x, y, alt, yaw, xdot, ydot, altdot, yawdot} 

  // channels_ptr (8-element array of PWM commands to generate in this function)
  // Channels for you to set:
  // [0] = thrust
  // [1] = roll
  // [2] = pitch
  // [3] = yaw
  

  // x, y, z(alt) uses PID
  // yaw uses only P
  
  printf("waypts:%f,%f,%f\n", set_points[0], set_points[1], set_points[2]);
  x_err = set_points[0] - pose[0];
  y_err = set_points[1] - pose[1];
  alt_err = set_points[2] - pose[2];
  yaw_err = set_points[3] - pose[3];
    

  x_dot_err = set_points[4] - pose[4];
  y_dot_err = set_points[5] - pose[5];
  alt_dot_err = set_points[6] - pose[6];

  x_i_err += x_err*0.08;
  y_i_err += y_err*0.08;
  alt_i_err += alt_err*0.08;

  PID_msg.utime = utime_now();
  PID_msg.err[0] = x_err;
  PID_msg.err[1] = y_err;
  PID_msg.err[2] = alt_err;
  PID_msg.err[3] = yaw_err;
  PID_msg.err_dot[0] = x_dot_err;
  PID_msg.err_dot[1] = y_dot_err;
  PID_msg.err_dot[2] = alt_dot_err;
  PID_msg.err_i_raw[0] = x_i_err;
  PID_msg.err_i_raw[1] = y_i_err;
  PID_msg.err_i_raw[2] = alt_i_err;

    
  if (x_i_err > x_i_thres) x_i_err = x_i_thres;
  if (x_i_err < -x_i_thres) x_i_err = -x_i_thres;
  if (y_i_err > y_i_thres) y_i_err = y_i_thres;
  if (y_i_err < -y_i_thres) y_i_err = -y_i_thres;
  if (alt_i_err > z_i_thres) alt_i_err = z_i_thres;
  if (alt_i_err < -z_i_thres) alt_i_err = -z_i_thres;
  PID_msg.err_i_threshed[0] = x_i_err;
  PID_msg.err_i_threshed[1] = y_i_err;
  PID_msg.err_i_threshed[2] = alt_i_err;



  acc_x = GAIN_X*x_err + GAIN_X_DOT*x_dot_err + GAIN_X_I*x_i_err;
  acc_y = GAIN_Y*y_err + GAIN_Y_DOT*y_dot_err + GAIN_Y_I*y_i_err;


  acc_z = GAIN_Z*alt_err + GAIN_Z_DOT*alt_dot_err + GAIN_Z_I*alt_i_err;
  PID_msg.desire_acc[0] = acc_x;
  PID_msg.desire_acc[1] = acc_y;
  PID_msg.desire_acc[2] = acc_z;

  //printf("acc_z:%f\n", acc_z);
  //printf("alt_err:%f\n", alt_err);
  //printf("alt_err_dot:%f\n", alt_dot_err);
  //  printf("Alt_err: %f, Alt_d_err: %f, Alt_i_err: %f\n", alt_err, alt_dot_err, alt_i_err);
  // due to acc_z is negative
  // pitch_d = -(acc_x*cos(pose[3]) + acc_y*sin(pose[3])) / ( - 9.8);
  pitch_d = acc_x/9.8;
  roll_d = acc_y/9.8;
  // roll_d = -(acc_y*cos(pose[3]) - acc_x*sin(pose[3])) / ( - 9.8);

  PID_msg.desire_angle[0] = roll_d;
  PID_msg.desire_angle[1] = pitch_d;

  //  printf("pitch_d:%f\n", pitch_d);
  //printf("acc_x: %f\n", acc_x);
  //printf("err_x:%f\n", x_err);
  //printf("err_x_dot:%f\n", x_dot_err);
  yaw_vel = GAIN_YAW*yaw_err;
  PID_msg.desire_yaw_vel = yaw_vel;

  // translate the control command to the quadrotor readable value
  channels_ptr[0] = (unsigned int) (A_z*acc_z + thrust_PWM_base);
  channels_ptr[1] = (unsigned int) (A_r*roll_d + roll_PWM_base);
  //printf("Channel[0]: %d\n", channels_ptr[0]);
  channels_ptr[2] = (unsigned int) (A_p*pitch_d + pitch_PWM_base);
  channels_ptr[3] = (unsigned int) (A_yaw*yaw_vel + yaw_PWM_base);

  if (channels_ptr[0] > thrust_PWM_up) channels_ptr[0] = thrust_PWM_up;
  if (channels_ptr[0] < thrust_PWM_down) channels_ptr[0] = thrust_PWM_down;

  //printf("Channel[1] before  threshed: %d\n", channels_ptr[1]);

  if (channels_ptr[1] < roll_PWM_right) channels_ptr[1] = roll_PWM_right;
  if (channels_ptr[1] > roll_PWM_left) channels_ptr[1] = roll_PWM_left;
  //printf("Channel[1] after threshed: %d\n", channels_ptr[1]);

  if (channels_ptr[2] > pitch_PWM_forward) channels_ptr[2] = pitch_PWM_forward;
  if (channels_ptr[2] < pitch_PWM_backward) channels_ptr[2] = pitch_PWM_backward;

  if (channels_ptr[3] < yaw_PWM_cw) channels_ptr[3] = yaw_PWM_cw;
  if (channels_ptr[3] > yaw_PWM_ccw) channels_ptr[3] = yaw_PWM_ccw;

  PID_msg.pose[0] = pose[0];
  PID_msg.pose[1] = pose[1];
  PID_msg.pose[2] = pose[2];
  PID_msg.pose[3] = pose[3];
  PID_msg.pose[4] = pose[4];
  PID_msg.pose[5] = pose[5];
  PID_msg.pose[6] = pose[6];
  PID_msg.pose[7] = pose[7];

  PID_t_publish(lcm, "PID_MESSAGES",&PID_msg);


  return;

}
