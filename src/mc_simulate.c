// run_motion_capture.c thread
//
// Start by including Optitrack code (PacketClient.cpp) with
// main() function commented out.  Pertinent main() commands pasted into
// top-level thread function run_motion_capture in this file.
//

#define EXTERN extern
#include "../include/quadcopter_main.h"

#include <stdlib.h>
// Students: Comment the following definition out unless you're testing our geofencing software
// #define GEOFENCE_AXES

void *mc_simulate(void *userdata){

  struct motion_capture_obs mc; // Local copy of mcap_obs object (to update)
      optitrack_t opti_msg;
      time_t t;
  while(1) {

    srand((unsigned) time(&t));
    mc.time = ((double)utime_now())/1000000;
    mc.pose[0] = (float)(rand()%2000)/1000.0;
    mc.pose[1] = (float)(rand()%2000)/1000.0;
    mc.pose[2] = -(float)(rand()%2000)/1000.0;
    mc.pose[3] = (float)(rand()%1000)/1000.0;
    mc.pose[4] = (float)(rand()%1000)/1000.0;
    mc.pose[5] = (float)(rand()%1000)/1000.0;

    opti_msg.pose[0] = mc.pose[0];
    opti_msg.pose[1] = mc.pose[1];
    opti_msg.pose[2] = mc.pose[2];
    opti_msg.pose[3] = mc.pose[3];
    opti_msg.pose[4] = mc.pose[4];
    opti_msg.pose[5] = mc.pose[5];

    optitrack_t_publish(lcm, "MOTION_CAPTURE", &opti_msg);


    pthread_mutex_lock(&mcap_mutex);
    memcpy(mcap_obs+1, mcap_obs, sizeof(struct motion_capture_obs));
    memcpy(mcap_obs, &mc, sizeof(struct motion_capture_obs));
    pthread_mutex_unlock(&mcap_mutex);

    usleep(10000);
  }

  // fclose(mcap_txt);
  return NULL;
}
