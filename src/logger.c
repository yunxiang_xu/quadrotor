#include <stdio.h>
#include <inttypes.h>
#include <lcm/lcm.h>
#include "../include/lcmtypes/lcmtypes_c/channels_t.h"
#include "../include/lcmtypes/lcmtypes_c/optitrack_t.h"
#include <time.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>

time_t rawtime;
struct tm* info;
char channel_TX_fname[50], channel_RX_fname[50], mc_fname[50];
int64_t utime_now (void){
    struct timeval tv;
    gettimeofday (&tv, NULL);
    return (int64_t) tv.tv_sec * 1000000 + tv.tv_usec;
}

void mc_handler(const lcm_recv_buf_t *rbuf, const char * channel, 
        const optitrack_t* msg, void * user) {

	FILE* logger;
	logger = fopen(mc_fname, "a");
	if(logger == NULL) {
		perror("File open error! \n");
		exit(0);
	}
	
	//timestamp, accel[0]~[2], angle[0]~[2], angular_vel[0]~[2]
	fprintf(logger, "%" PRId64 ",%f, %f, %f, %f, %f, %f\n", 
		utime_now(), msg->pose[0], msg->pose[1], msg->pose[2], msg->pose[3], msg->pose[4], msg->pose[5]);
	fclose(logger);

}

void channels_TX_handler(const lcm_recv_buf_t *rbuf, const char * channel, 
        const channels_t* msg, void * user) {

	FILE* logger;
	logger = fopen(channel_TX_fname, "a");
	if(logger == NULL) {
		perror("File open error! \n");
		exit(0);
	}
	
	//timestamp, accel[0]~[2], angle[0]~[2], angular_vel[0]~[2]
	fprintf(logger, "%" PRId64 ",%d, %d, %d, %d, %d\n", 
		utime_now(), (int)msg->channels[0],(int)msg->channels[1],(int)msg->channels[2],(int)msg->channels[3],(int)msg->channels[7]);
	fclose(logger);

}


void channels_RX_handler(const lcm_recv_buf_t *rbuf, const char * channel, 
        const channels_t* msg, void * user) {

	FILE* logger;
	logger = fopen(channel_RX_fname, "a");
	if(logger == NULL) {
		perror("File open error! \n");
		exit(0);
	}
	
	//timestamp, accel[0]~[2], angle[0]~[2], angular_vel[0]~[2]
	fprintf(logger, "%" PRId64 ",%d\n", 
		utime_now(), (int)msg->channels[7]);
	fclose(logger);

}


int main(int argc, char* argv[]) {

	time(&rawtime);
	info = localtime(&rawtime);
	strftime(channel_TX_fname, 50, "../data/channel_TX_%d%H%M%S.txt", info);
	strftime(channel_RX_fname, 50, "../data/channel_RX_%d%H%M%S.txt", info);

	strftime(mc_fname, 50, "../data/mc_%d%H%M%S.txt", info);

	printf("%s\n",channel_TX_fname);
	printf("%s\n",channel_RX_fname);
	printf("%s\n",mc_fname);


	lcm_t* lcm = lcm_create(NULL);

	channels_t_subscribe(lcm, "CHANNELS_1_TX", channels_TX_handler, NULL);
	channels_t_subscribe(lcm, "CHANNELS_1_RX", channels_RX_handler, NULL);
	optitrack_t_subscribe(lcm, "MOTION_CAPTURE", mc_handler, NULL);

	while(1) {
		lcm_handle(lcm);
	}

	lcm_destroy(lcm);
	return 0;
}