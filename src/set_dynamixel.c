#define EXTERN extern
#include "../include/quadcopter_main.h"
#define ID1_LOCK_ANGLE 100
#define ID2_LOCK_ANGLE 100
int tight_flag = 0;
void * set_dynamixel(void * var){
    /*
    int i = 0;
    for(;i < 100;i++){
        switch(i % 7){
            case 0:
                printf("Go To Position 10 deg with 0.4 speed\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_angle = 10.0;
                bus.servo[0].cmd_speed = 0.4;
                bus.servo[0].cmd_flag = CMD;
                pthread_mutex_unlock(&dynamixel_mutex);
                break;
            case 1:
                printf("Go To Position 100 deg with 0.1 speed\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_angle = 100.0;
                bus.servo[0].cmd_speed = 0.1;
                bus.servo[0].cmd_flag = CMD;
                pthread_mutex_unlock(&dynamixel_mutex);
                break;
            case 2:
                printf("Change to Wheel Mode - Stop the Servo\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_mode = WHEEL;
                bus.servo[0].cmd_flag = MODE;
                pthread_mutex_unlock(&dynamixel_mutex);
                break;
            case 3:
                printf("Command 0.3 speed\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_speed = 0.3;
                bus.servo[0].cmd_flag = CMD;
                pthread_mutex_unlock(&dynamixel_mutex);
                break;
            case 4:
                printf("Request Status:\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_flag = STATUS;
                pthread_mutex_unlock(&dynamixel_mutex);
                break;
            case 5:
                printf("Command -0.3 speed\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_speed = -0.3;
                bus.servo[0].cmd_flag = CMD;
                pthread_mutex_unlock(&dynamixel_mutex);
                break;
            case 6:
                printf("Change to Joint Mode - Go to default position\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_mode = JOINT;
                bus.servo[0].cmd_flag = MODE;
                pthread_mutex_unlock(&dynamixel_mutex);
                break;
        }


        fflush(stdout);
        sleep(5);

        // If we requested Status Print Status
        pthread_mutex_lock(&dynamixel_mutex);
        if(i%7 == 4) Dynam_PrintStatus(&(bus.servo[0]));
        pthread_mutex_unlock(&dynamixel_mutex);
    }
    */

    while(1) {
        if (grip){
            actGrip();
        }
        
        if (release){
        
	        actRelease();
        }

    }
}
void actGrip(){
	
    float curr_angle_1, curr_angle_2;
    float angle_thres = 0;
    if (abs(ID1_LOCK_ANGLE - curr_angle_1) < angle_thres &
        abs(ID2_LOCK_ANGLE - curr_angle_2) < angle_thres & tight_flag == 0){
        pthread_mutex_lock(&dynamixel_mutex);
        printf("switch to joint mode\n");
        bus.servo[0].cmd_mode = JOINT;
        bus.servo[1].cmd_mode = JOINT;
        bus.servo[0].cmd_flag = MODE;
        bus.servo[1].cmd_flag = MODE;
        pthread_mutex_unlock(&dynamixel_mutex);
        tight_flag = 1;
        sleep(1);
    }

    if (tight_flag == 1){
        printf("stop the servo\n");
        pthread_mutex_lock(&dynamixel_mutex);
        //bus.servo[0].cmd_angle = curr_angle_1;
        //bus.servo[1].cmd_angle = curr_angle_2;
        	
        bus.servo[0].cmd_speed = 0.4;
        bus.servo[1].cmd_speed = 0.25;
	
	bus.servo[0].cmd_torque = 0.4;
	bus.servo[1].cmd_torque = 0.3;
        bus.servo[0].cmd_flag = CMD;
        bus.servo[1].cmd_flag = CMD;
        pthread_mutex_unlock(&dynamixel_mutex);
	sleep(1);
    }
    else{
        //set the mode
        printf("set to wheel mode\n");
        //pthread_mutex_lock(&dynamixel_mutex);
        //bus.servo[0].cmd_mode = WHEEL;
        //bus.servo[1].cmd_mode = WHEEL;
        //bus.servo[0].cmd_flag = MODE;
        //bus.servo[1].cmd_flag = MODE;
        //pthread_mutex_unlock(&dynamixel_mutex);
        //sleep(1);
        
        
        //close the gripper
        printf("set speed command\n");
        pthread_mutex_lock(&dynamixel_mutex);
        bus.servo[0].cmd_speed = 0.5;
        bus.servo[1].cmd_speed = 0.5;
        bus.servo[0].cmd_torque = 0.5;
        bus.servo[1].cmd_torque = 0.5;
        bus.servo[0].cmd_flag = CMD;
        bus.servo[1].cmd_flag = CMD;
        pthread_mutex_unlock(&dynamixel_mutex);

        sleep(5);
        tight_flag = 1;
    }
    
    pthread_mutex_lock(&dynamixel_mutex);
    bus.servo[0].cmd_flag = STATUS;
    bus.servo[1].cmd_flag = STATUS;
    //Dynam_PrintStatus(&(bus.servo[0]));
    //Dynam_PrintStatus(&(bus.servo[1]));
    curr_angle_1 = bus.servo[0].cur_angle;
    curr_angle_2 = bus.servo[1].cur_angle;
    printf("angle for id 1: %f\n",curr_angle_1);
    printf("angle for id 2: %f\n",bus.servo[1].cur_angle);
    pthread_mutex_unlock(&dynamixel_mutex);
}

void actRelease(){
    tight_flag = 0;
    //open the gripper
    printf("release set to wheel mode\n");
    pthread_mutex_lock(&dynamixel_mutex);
    //bus.servo[0].cmd_mode = WHEEL;
    //bus.servo[1].cmd_mode = WHEEL;
    bus.servo[0].cmd_speed = -0.6;
    bus.servo[1].cmd_speed = -0.6;
    bus.servo[0].cmd_torque = 0.4;
    bus.servo[1].cmd_torque = 0.4;
    bus.servo[0].cmd_flag = CMD;
    bus.servo[1].cmd_flag = CMD;
    pthread_mutex_unlock(&dynamixel_mutex);

    sleep(1);

    pthread_mutex_lock(&dynamixel_mutex);
    bus.servo[0].cmd_flag = STATUS;
    bus.servo[1].cmd_flag = STATUS;
    //Dynam_PrintStatus(&(bus.servo[0]));
    //Dynam_PrintStatus(&(bus.servo[1]));


    pthread_mutex_unlock(&dynamixel_mutex);

    sleep(5);
    release = 0;
}



