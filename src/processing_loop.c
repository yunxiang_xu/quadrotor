// Processing loop (contains geofence code as an example only)
// processing_loop():  Top-level thread function
// update_set_points():  Geofencing reference points (may be useful as a guide)
// processing_loop_initialize():  Set up data processing / geofence variables
// read_config:  Read configuration parameters from a file (customize please)
//
#define EXTERN extern
#define PROC_FREQ 100 // in Hz

#define norm_square_thres 0.005
#define yaw_thres 0.15
#include "../include/quadcopter_main.h"
void read_config(char *);
int processing_loop_initialize();
/*
 * State estimation and guidance thread 
*/
void *processing_loop(void *data){
  printf("%d\n", __LINE__);
  int hz = PROC_FREQ;
  processing_loop_initialize();
  printf("%d\n", __LINE__);
  int time_counter = 0;

  // Local copies
  struct motion_capture_obs *mcobs[2];  // [0] = new, [1] = old
  struct state localstate;
  
  //initialize first way point
  // if no way point has been set
  // initialzied way point as current way point
  usleep(500000); // wait for mcapture data
  printf("if set_wypt_index is 0:\n", set_wypt_index);
  if (set_wypt_index == 0) {
    printf("data:%f, %f, %f, %f", mcap_obs->pose[0], mcap_obs->pose[1], mcap_obs->pose[2], mcap_obs->pose[3]);
    pthread_mutex_lock(&mcap_mutex);
      list[0].x = mcap_obs->pose[0];
      list[0].y = mcap_obs->pose[1];
      list[0].z = mcap_obs->pose[2];
      list[0].yaw = mcap_obs->pose[3];
      pthread_mutex_unlock(&mcap_mutex);
      set_wypt_index++;
      update_set_points(localstate.pose, localstate.set_points,0);
  } else {

    update_set_points(localstate.pose, localstate.set_points,0);

  }
  printf("%d\n", __LINE__);

  while (1) {  // Main thread loop

    pthread_mutex_lock(&mcap_mutex);
    mcobs[0] = mcap_obs;
    if(mcap_obs[1].time > 0) mcobs[1] = mcap_obs+1;
    else               mcobs[1] = mcap_obs;

    //check if it's in auto mode
    localstate.fence_on = state->fence_on;
    
    pthread_mutex_unlock(&mcap_mutex);
  
    localstate.time = mcobs[0]->time;
    localstate.pose[0] = mcobs[0]->pose[0]; // x position
    localstate.pose[1] = mcobs[0]->pose[1]; // y position
    localstate.pose[2] = mcobs[0]->pose[2]; // z position / altitude
    localstate.pose[3] = mcobs[0]->pose[5]; // yaw angle
    localstate.pose[4] = 0;
    localstate.pose[5] = 0;
    localstate.pose[6] = 0;
    localstate.pose[7] = 0;
    

    // Estimate velocities from first-order differentiation
    double mc_time_step = mcobs[0]->time - mcobs[1]->time;
    // printf("time_step:%f\n", mc_time_step);
    if(mc_time_step > 1.0E-7 && mc_time_step < 1) {
      
      localstate.pose[4] = (mcobs[0]->pose[0]-mcobs[1]->pose[0])/mc_time_step;
      localstate.pose[5] = (mcobs[0]->pose[1]-mcobs[1]->pose[1])/mc_time_step;
      localstate.pose[6] = (mcobs[0]->pose[2]-mcobs[1]->pose[2])/mc_time_step;
      localstate.pose[7] = (mcobs[0]->pose[5]-mcobs[1]->pose[5])/mc_time_step;

      localstate.data_lost = 0;

    } else {
      //if time_step greater than 1s, optitrack data drops off
      localstate.data_lost = 1;
      printf("lost!\n");

    }


    // printf("%d\n", __LINE__);

  // Geofence activation code
  // turn fence on --> may be useful to control your autonomous controller
    if(localstate.fence_on == 0){
      
      
      // ADD YOUR CODE HERE
      
    } // end if (localstate.fence_on == 0)

    // else if fence is on and has been on for desired length of time

    //only read here
    ///*******************************
    else {
      // update desired state
      // check if need to update setpoints
      //printf("Fence On\n");
      /***************************************************************************
        Test specific set point with PID
      *****************************************************************************/

        // localstate.set_points[0] = 0;
        // localstate.set_points[1] = 0;
        // localstate.set_points[2] = -1.5;
        // localstate.set_points[3] = 0;

      norm_square = 0.0;
      // printf("current waypoints:%d,  %f, %f, %f, %f\n", waypoint_index, localstate.set_points[0], localstate.set_points[1], localstate.set_points[2], localstate.set_points[3]);
      for (int i = 0; i < 3; i ++) {
        norm_square += (localstate.pose[i] - localstate.set_points[i])*(localstate.pose[i] - localstate.set_points[i]);
      }
      //printf("Err norm:%f\n", norm_square );
      if (norm_square < norm_square_thres && abs(localstate.pose[4] - localstate.set_points[4]) < yaw_thres) {
	//printf("arrive way point! \n");
        if (grip) {
          // activate the gripper
	  //          printf("gripping!\n");
          //  usleep(2000000);

          if (release) {

            // if read the release command, release the gripper
            grip = 0;
            //usleep(2000000);

          } else 
	    printf("gripping!\n");


        }

        // if arrived a way point and it's not a point for gripping, wait for 100 count
        // and clear count
        if (time_counter > 100 && grip == 0) {
          update_set_points(localstate.pose, localstate.set_points,1);
          time_counter = 0;
        // if arrived a way point and it's a point for gripping, wait for 400 count
        // and clear count
        //} else if (time_counter > 400 && grip == 1) {
          // if it's gripping state, it shouldn't update way point
	  // update_set_points(localstate.pose, localstate.set_points,1);
          //time_counter = 0;
	  }
        // if count not achieved, increment time_counter
        else {
          time_counter ++;
	  // printf("arrived waypoint counting...\n");
        }
      
      } else {
        time_counter = 0;
      }

    }

    // Copy to global state (minimize total time state is locked by the mutex)
    pthread_mutex_lock(&state_mutex);
    
    
    // copy the state to the global variable
    memcpy(state, &localstate, sizeof(struct state));
    
    
    pthread_mutex_unlock(&state_mutex);
    usleep(1000000/hz);

  } // end while processing_loop()

  return 0;
}

/**
 * update_set_points()
 */
int update_set_points(double* pose, float *set_points, int first_time){
  //Add your code here to generate proper reference state for the controller
  if (waypoint_index >= set_wypt_index) {
    while(1)  {
      printf("All way point achieved\n");
      usleep(5000000);
    }
      return 1;
  }
  set_points[0] = list[waypoint_index].x;
  set_points[1] = list[waypoint_index].y;
  set_points[2] = list[waypoint_index].z;
  set_points[3] = list[waypoint_index].yaw;
  grip = list[waypoint_index].grip;

  waypoint_index++;
  printf("set points updated!\n");
  printf("new way point: x:%f, y:%f, z:%f, yaw:%f \n",set_points[0],set_points[1],set_points[2],set_points[3]);

  return 0;

}


/**
 * processing_loop_initialize()
*/
int processing_loop_initialize() {
  
  // Initialize state struct
  state = (state_t*) calloc(1,sizeof(*state));
  state->time = ((double)utime_now())/1000000;
  memset(state->pose,0,sizeof(state->pose));
  printf("%d\n", __LINE__);

  // // Read configuration file
  // char blah[] = "config.txt";
  // read_config(blah);
  printf("%d\n", __LINE__);

  mcap_obs[0].time = state->time;
  mcap_obs[1].time = -1.0;  // Signal that this isn't set yet
  printf("%d\n", __LINE__);

  // Initialize Altitude Velocity Data Structures
  memset(diff_z, 0, sizeof(diff_z));
  memset(diff_z_med, 0, sizeof(diff_z_med));

  // Fence variables
  state->fence_on = 0;
  memset(state->set_points,0,sizeof(state->set_points));
  state->time_fence_init = 0;
  waypoint_index = 0;
  set_wypt_index = 0;

  //initialized integral terms in PID
  x_i_err = 0;
  y_i_err = 0;
  alt_i_err = 0;

  // boolean terms for gripper gripping and release
  grip = 0;
  release = 0;

  return 0;

}

// Grip = 1 means you want to set this state as the state you want to activate the gripper
void set_waypt(const double* pose, int Grip) {

    // pose[0] x position
    // pose[1] y position
    // pose[2] z position / altitude
    // pose[3] yaw angle

  printf("current x:%f\n", pose[0]);  
  list[set_wypt_index].x = pose[0];
  list[set_wypt_index].y = pose[1];
  list[set_wypt_index].z = pose[2];
  list[set_wypt_index].yaw = pose[3];
  list[set_wypt_index].grip = Grip;
  set_wypt_index ++;

}

void* key_board(void* data) {

  //testing purpose
  // processing_loop_initialize();


  while (1) {
    char c = getchar();
  printf("%d\n", __LINE__);

    if (c == 'w') {
      set_waypt(state->pose, 0);
      printf("waypoint list:%d\n", set_wypt_index);
      for (int i = 0; i < set_wypt_index; i ++) {
        printf("%f, %f, %f, %f\n", list[i].x, list[i].y, list[i].z, list[i].yaw);
      }
      printf("--------------------\n");
      printf("set way point, not gripping\n");
    } else if (c == 'c') {
    printf("%d\n", __LINE__);

      set_waypt(state->pose, 1);
    printf("%d\n", __LINE__);
      printf("waypoint list:%d\n", set_wypt_index);

      for (int i = 0; i < set_wypt_index; i ++) {
        printf("%f, %f, %f, %f\n", list[i].x, list[i].y, list[i].z, list[i].yaw);
      }
      printf("--------------------\n");

      printf("set way point, gripping\n");

    } else if (c == 'r') {

      release = 1;
    } else {

      // release = 0;
    }
    
    printf("Number of waypoints: %d\n", set_wypt_index);
  }

  return NULL;

}
