//
// Top-level program for quadcopter
//
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <unistd.h>
#include <pthread.h>

#define EXTERN   // main() program definition

#include "../include/quadcopter_main.h"

/*  ########################################
    ###    QUADCOPTER MAIN FUNCTION      ### 
    ########################################

    Code has several threads. 
    Comment the ones you do not need.
    List below show the threads you NEED to run it.

    ### FOR THE QUADROTOR TO FLY ###
    -> lcm_thread_loop
    -> processing_loop
    -> run_motion_capture
    -> (if you use servos) run_dynamixel_comm
    -> (if you use imu)    run_imu

    ### FOR TESTING MOTION CAPTURE ONLY ###
    -> run_motion_capture

    ### FOR TESTING SERVOS ONLY (NOT CONNECTED TO YOUR GRIPPER!) ###
    -> run_dynamixel_comm
    -> set_dynamixel
*/

int main() {


  lcm = lcm_create(NULL);

  // Initialize the data mutexes
  pthread_mutex_init(&imu_mutex, NULL);
   pthread_mutex_init(&mcap_mutex, NULL);
  pthread_mutex_init(&state_mutex, NULL);
  pthread_mutex_init(&dynamixel_mutex, NULL);

  // Start the threads

  // subscribe control command from timing block;
  // 
  pthread_t lcm_thread;
  
  pthread_t processing_thread;
  pthread_t imu_thread;
  pthread_t motion_capture_thread;
  pthread_t dynamixel_comm_thread;
  pthread_t dynamixel_set_thread;
  pthread_t key_board_thread;
  pthread_t mc_simulate_thread;
  pthread_t processing_PID_thread;


  pthread_create(&lcm_thread, NULL, lcm_thread_loop, NULL);
  // pthread_create(&key_board_thread, NULL, key_board, NULL);
  // pthread_create(&processing_thread, NULL, processing_loop, NULL);
  pthread_create(&processing_PID_thread, NULL, processing_PID, NULL);
  //  pthread_create(&mc_simulate_thread, NULL, mc_simulate, NULL);
  // UNCOMMENT TO USE IMU
  pthread_create(&imu_thread, NULL, run_imu, NULL);

   pthread_create(&motion_capture_thread, NULL, run_motion_capture, NULL);
  // pthread_create(&dynamixel_comm_thread, NULL, run_dynamixel_comm, NULL);
  // pthread_create(&dynamixel_set_thread, NULL, set_dynamixel, NULL);

  // Join threads upon completetion
  pthread_join(lcm_thread, NULL);
  // pthread_join(processing_thread, NULL);
  pthread_join(processing_PID_thread, NULL);
  // UNCOMMENT TO USE IMU
  pthread_join(imu_thread, NULL);
  // pthread_join(key_board_thread, NULL);

  pthread_join(motion_capture_thread, NULL);
  // pthread_join(dynamixel_comm_thread, NULL);
  // pthread_join(dynamixel_set_thread, NULL);
  // pthread_join(mc_simulate_thread, NULL);
  lcm_destroy(lcm);
 
  // float* pose = (float*) calloc(8,sizeof(float));
  // float* setPoint = (float*) calloc(8,sizeof(float));
  // int16_t* channels = (int16_t*) malloc(4*sizeof(int16_t));

  // setPoint[2] = 1;
  // auto_control(pose, setPoint, channels);
  // for(int j = 0; j < 4; j ++) {
  //   printf("channel %d, %d\n",j, channels[j] );
  // }
  // 

  return 0;

}

/*
  LCM processing (top-level loop) -- Only use to talk with BLOCKS
  get control command values of transimitter  from timing block
*/
void *lcm_thread_loop(void *data){

  channels_t_subscribe(lcm, "CHANNELS_1_RX", channels_handler, lcm);
  while(1)
    lcm_handle(lcm);
  return 0;

}
