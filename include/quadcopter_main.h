#ifndef QUADCOPTER_GLOBALS
#define QUADCOPTER_GLOBALS

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <unistd.h>
#include <pthread.h>
#include <math.h>

#ifdef __cplusplus
extern "C" {
#endif

//Use this in your code:  #define EXTERN extern

#include <lcm/lcm.h>
#include "../include/lcmtypes/lcmtypes_c/channels_t.h"
#include "../include/lcmtypes/lcmtypes_c/optitrack_t.h"
#include "../include/lcmtypes/lcmtypes_c/PID_t.h"
#include "../include/lcmtypes/lcmtypes_c/imu_data_t.h"

#include "../include/quadcopter_struct.h"
#include "../include/bbblib/bbb.h"

// Primary threads
#include "../include/run_imu.h"
#include "../include/run_motion_capture.h"
#include "../include/util.h"


#define MAX_WYPT_NUM 15
/* Global Variables with mutexes for sharing */

EXTERN imu_t imudata;
EXTERN struct motion_capture_obs mcap_obs[2];
EXTERN state_t *state;
EXTERN DynamBus bus;

/* Mutexes */
EXTERN pthread_mutex_t imu_mutex; 
EXTERN pthread_mutex_t mcap_mutex;  
EXTERN pthread_mutex_t state_mutex;
EXTERN pthread_mutex_t dynamixel_mutex;

/* Global variables that are not used in multiple threads (no mutex use) */
EXTERN FILE *mcap_txt, *block_txt, *imu_txt; // Output data files

EXTERN int imu_mode, mcap_mode;
EXTERN struct imu_data *imu;
EXTERN struct wypt_list list[MAX_WYPT_NUM];
EXTERN DynamSetup dynam;

// Top-level thread function declarations
void *lcm_thread_loop(void *);
void *processing_loop(void *);
void *processing_PID(void *);

void *key_board(void *);
void *run_imu(void *);
void *run_motion_capture(void *);

/* Function declarations needed for multiple files */
void channels_handler(const lcm_recv_buf_t *rbuf, const char *channel,
		      const channels_t *msg, void *userdata);
void auto_control(float *pose, float *set_points, int16_t *channels_ptr);

void actGrip();
void actRelease();
//////////////////////////////////////////
///  Geofence stuff for testing

#define NUM_SAMPLES_MED_ALT 10
#define NUM_SAMPLES_AVG_ALT 20

EXTERN float KP_thrust, KI_thrust, KD_thrust;
EXTERN float KP_pitch, KI_pitch, KD_pitch;
EXTERN float KP_roll, KI_roll, KD_roll;
EXTERN float KP_yaw, KI_yaw, KD_yaw;

EXTERN double diff_z[NUM_SAMPLES_MED_ALT], diff_z_med[NUM_SAMPLES_AVG_ALT];
EXTERN double start[2];
EXTERN double ang_buf;
EXTERN double fence_penalty_length;
EXTERN float x_err, x_dot_err, x_i_err;
EXTERN float y_err, y_dot_err, y_i_err;
EXTERN float yaw_err, yaw_dot_err;
EXTERN float alt_err, alt_dot_err, alt_i_err;

EXTERN float acc_x, acc_y, acc_z;
EXTERN float pitch_d, roll_d, yaw_vel;
// max desired velocity for fence corrections
EXTERN float max_vel, max_step;

EXTERN double alt_low_fence, alt_high_fence;
EXTERN double x_pos_fence, x_neg_fence, y_pos_fence, y_neg_fence;
EXTERN double alt_prev;

EXTERN float norm_square;
//define a list to store waypoints
//Use waypoint_index as index
EXTERN int waypoint_index;
EXTERN int set_wypt_index;
EXTERN int grip;
EXTERN int release;
EXTERN lcm_t* lcm;
EXTERN PID_t PID_msg;

//////////////////////////////////////////////////////////////////////////////
// Geofence function
int update_set_points(double* pose, float* set_points, int first_time);

// Define outer loop controller 
// PWM signal limits and neutral (baseline) settings

// THRUST
#define thrust_PWM_up 1600 // Upper saturation PWM limit.
#define thrust_PWM_base 1530 // Zero z_vela PWM base value. 
#define thrust_PWM_down 1400 // Lower saturation PWM limit. 
  
// ROLL
#define roll_PWM_left 1650  // Left saturation PWM limit.
#define roll_PWM_base 1510  // Zero roll_dot PWM base value. 
#define roll_PWM_right 1350 //Right saturation PWM limit. 

// PITCH
#define pitch_PWM_forward 1650  // Forward direction saturation PWM limit.
#define pitch_PWM_base 1490 // Zero pitch_dot PWM base value. 
#define pitch_PWM_backward 1350 // Backward direction saturation PWM limit. 

// YAW
#define yaw_PWM_ccw 1600 // Counter-Clockwise saturation PWM limit (ccw = yaw left).
#define yaw_PWM_base 1510 // Zero yaw_dot PWM base value. 
#define yaw_PWM_cw 1400 // Clockwise saturation PWM limit (cw = yaw right). 

#ifdef __cplusplus
}
#endif

#endif
